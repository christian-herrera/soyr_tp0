#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

int main(){

    struct sDatos{
        uint32_t ID;
        uint16_t presion;
        int16_t temp;
        uint16_t precipitaciones;
        uint8_t hum;
        uint32_t t_unix;
    } Datos;


    




    FILE *arch, *archCSV;
    arch = fopen("datos.bin","rb");
    archCSV = fopen("datos.csv", "w");

    if (arch == NULL || archCSV == NULL)
        exit(1);


    while(1){
        fread(&Datos, sizeof(Datos), 1, arch);

        // strftime cuenta desde el 1970 por lo tanto, agrego los segundos
        // correspondientes a la direferencia entre los años.
        time_t tiempo = Datos.t_unix + 946080000;
        struct tm *fecha = gmtime(&tiempo);
        char fecha_s[15], hora_s[15];
        strftime(fecha_s, sizeof(fecha_s), "%d/%m/%Y", fecha);
        strftime(hora_s, sizeof(hora_s), "%H:%M:%S", fecha);

        printf( "%i %.1f %.1f %.1f %hhu %s - %s\n", Datos.ID, Datos.presion/10.0, Datos.temp/10.0, Datos.precipitaciones/10.0, Datos.hum, fecha_s, hora_s);
        fprintf(archCSV, "%i,%.1f,%.1f,%.1f,%hhu,%s,%s\n", Datos.ID, Datos.presion/10.0, Datos.temp/10.0, Datos.precipitaciones/10.0, Datos.hum, fecha_s, hora_s);
        if(feof(arch))
            break;

    }

    fclose(arch);
    fclose(archCSV);
    return 0;
}